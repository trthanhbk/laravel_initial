<?php namespace Prototype\ServiceProviders;

use Illuminate\Support\ServiceProvider;
use Laracasts\Validation\FormValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
* Class ExceptionProvider
*/
class ExceptionsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->registerCommonException();
    }

    private function loggingHelper($msg, $url)
    {
        \Log::error($msg);
        return \View::make('errors.' . $url);
    }
    /**
    * Register Common Exception
    */
    public function registerCommonException()
    {
        // Exception
        $this->app->error(function (\Exception $e, $code) {
            return $this->loggingHelper("error: " .$e->getMessage(), 'customError');
        });

        // PDOException
        $this->app->error(function (\PDOException $e, $code) {
            return $this->loggingHelper("error: " .$e->getMessage() . " --- From url: " . \Request::url(), 'customError');
        });

        // Form Validation Exception
        $this->app->error(function (FormValidationException $e, $code) {
            return \Redirect::back()
            ->withInput()
            ->withErrors($e->getErrors())
            ->withMessage('The following errors occurred');
        });

        // Not Found Http Exception
        $this->app->error(function (NotFoundHttpException $e, $code) {
            return $this->loggingHelper('NotFoundHttpException Route: ' . \Request::url(), '404Error');
        });
    }
}
