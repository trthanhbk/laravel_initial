<?php
namespace Prototype\Forms\User;

class SigninForm extends BaseUserForm
{
    
    protected $rules = array('username' => 'required|alpha_num|min:3', 'password' => 'required|alpha_num|between:6,12',);
}
