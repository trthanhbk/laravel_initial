<?php
namespace Prototype\Forms\User;

class ChangePassForm extends BaseUserForm
{
    
    protected $columns = ["password", "password_confirmation"];
}
