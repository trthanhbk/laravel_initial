<?php
namespace Prototype\Forms\User;

use Prototype\Forms\BaseForm;

abstract class BaseUserForm extends BaseForm
{
    
    protected $rules = array(
        'firstname'             => 'required|alpha_spaces|min:2',
        'lastname'              => 'required|alpha_spaces|min:2',
        'username'              => 'required|alpha_num|min:3|unique:users',
        'email'                 => 'required|email|unique:users',
        'password'              => 'required|alpha_num|between:6,12|confirmed',
        'password_confirmation' => 'required|alpha_num|between:6,12'
    );
}
