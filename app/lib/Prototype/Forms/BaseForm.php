<?php
namespace Prototype\Forms;

use Laracasts\Validation\FormValidator;

abstract class BaseForm extends FormValidator
{
    
    protected $columns = [];
            
    protected function _addUniqueId($id)
    {
        foreach ($this->rules as $key => $value) {
            $replaced = preg_replace("/(unique:[a-z]+)/", "$1,{$key},{$id}", $value);
            $this->rules[$key] = $replaced;
        }
    }

    protected function _selectColumns()
    {
        if (count($this->columns) > 0) {
            $rules = [];
            foreach ($this->columns as $key) {
                $rules[$key] = $this->rules[$key];
            }
            $this->rules = $rules;
        }
    }
    
    public function validate($formData)
    {
        $this->_selectColumns();
        parent::validate($formData);
    }
}
