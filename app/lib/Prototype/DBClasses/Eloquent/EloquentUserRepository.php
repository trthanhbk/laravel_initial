<?php
namespace Prototype\DBClasses\Eloquent;

use Prototype\Interfaces\UserRepository;
use Prototype\BaseClasses\AbstractEloquentRepository;

class EloquentUserRepository extends AbstractEloquentRepository implements UserRepository
{
    public function __construct(\User $model)
    {
        $this->model = $model;
    }
    
    public function updateProfile()
    {
        \Auth::user()->update(\Input::all());
    }
    
    public function updatePassword()
    {
        \Auth::user()->update(\Input::only("password"));
    }
}
