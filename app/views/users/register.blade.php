@extends('layouts.main')

@section('content')

{{ Form::open(array('action'=>'UsersController@postCreate', 'class'=>'form-signup')) }}
<h2 class="form-signup-heading">Please Register</h2>

<div class="form-group">{{ Form::text('firstname', null, array('class'=>'form-control', 'placeholder'=>'First Name')) }}
    {{ ErrorDisplay::getInstance()->displayIndividual($errors, "firstname") }}
</div>
<div class="form-group">{{ Form::text('lastname', null, array('class'=>'form-control', 'placeholder'=>'Last Name')) }}
    {{ ErrorDisplay::getInstance()->displayIndividual($errors, "lastname") }}
</div>
<div class="form-group">{{ Form::text('username', null, array('class'=>'form-control', 'placeholder'=>'Username')) }}
    {{ ErrorDisplay::getInstance()->displayIndividual($errors, "username") }}
</div>
<div class="form-group">{{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
    {{ ErrorDisplay::getInstance()->displayIndividual($errors, "email") }}
</div>
<div class="form-group">{{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
    {{ ErrorDisplay::getInstance()->displayIndividual($errors, "password") }}
</div>
<div class="form-group">{{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'Confirm Password')) }}
    {{ ErrorDisplay::getInstance()->displayIndividual($errors, "password_confirmation") }}
</div>

{{ Form::submit('Register', array('class'=>'btn btn-large btn-primary btn-block'))}}
{{ Form::close() }}

@stop