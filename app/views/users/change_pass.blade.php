@extends('layouts.dashboard')

@section('content')

<h1 class="page-header">{{ 'Change Password'  }}</h1>

{{ Form::model($currentUser, array('action' => 'UsersController@postChangePassword', 'class' => 'form-horizontal')) }}

    <div class="form-group">{{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
        {{ ErrorDisplay::getInstance()->DisplayIndividual($errors, "password") }}
    </div>
    <div class="form-group">{{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'Confirm Password')) }}
        {{ ErrorDisplay::getInstance()->DisplayIndividual($errors, "password_confirmation") }}
    </div>
    <div class="form-group">
        {{ Form::submit("Update", array('class'=>'btn btn-large btn-primary'))}}
    </div>

{{ Form::close() }}

@stop