@extends('layouts.dashboard')

@section('content')

<h1 class="page-header">{{ 'Profile Detail'  }}</h1>

{{ Form::model($currentUser, array('action' => 'UsersController@postUpdateProfile', 'class' => 'form-horizontal')) }}

    <div class="form-group text-right">{{ link_to_action('UsersController@getChangePassword', 'Change Password', [], ['class' => 'btn']) }}
    </div>
    <div class="form-group">{{ Form::text('username', null, array('class'=>'form-control', 'placeholder'=>'Username', 'readonly' => '')) }}
        {{ ErrorDisplay::getInstance()->DisplayIndividual($errors, "username") }}
    </div>
    <div class="form-group">{{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
        {{ ErrorDisplay::getInstance()->DisplayIndividual($errors, "email") }}
    </div>
    <div class="form-group">{{ Form::text('firstname', null, array('class'=>'form-control', 'placeholder'=>'First Name')) }}
        {{ ErrorDisplay::getInstance()->DisplayIndividual($errors, "firstname") }}
    </div>
    <div class="form-group">{{ Form::text('lastname', null, array('class'=>'form-control', 'placeholder'=>'Last Name')) }}
        {{ ErrorDisplay::getInstance()->DisplayIndividual($errors, "lastname") }}
    </div>
    <div class="form-group">{{ Form::submit("Update", array('class'=>'btn btn-large btn-primary'))}}
    </div>


{{ Form::close() }}

@stop