@extends('layouts.main')

@section('content')

<h2 class="form-signin-heading">Something Wrong.</h2>
<p>
<div>Oop, there was something wrong happen.</div>
<div>We will check and fix soon.</div>
<div>Sorry for any inconvenience.</div>
<div style="text-align: right">{{ \Carbon\Carbon::now(); }}</div>
</p>
@stop