@extends('layouts.main')

@section('content')

<h2 class="form-signin-heading">Wrong place.</h2>
<p>
<div>Oop, url you try to connect is invalid.</div>
<div>Please try another url.</div>
<div>Thank you very much.</div>
</p>
@stop