<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Laravel Prototype</title>

    <!-- Core CSS - Include with every page -->
    {{ HTML::style('asset/css/bootstrap.min.css') }}
    {{ HTML::style('asset/css/sb-admin.css')}}
    {{ HTML::style('asset/font-awesome/css/font-awesome.css')}}
    {{ HTML::style('asset/css/main.css')}}
    {{ HTML::style('asset/css/jquery.datetimepicker.css')}}
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/base/jquery-ui.css">
    @yield('head')
</head>

<body>

<div id="wrapper">

<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="{{ action('UsersController@getDashboard') }}">{{ "Dashboard" }}</a>
</div>
<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">

<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        {{ Auth::user()->getFullname() }} <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-user">
        <li>
            <a href="{{ action('UsersController@getDetail') }}"><i class="fa fa-sign-out fa-fw"></i>{{ "Profile Detail" }}</a>
            <a href="{{ action('UsersController@getLogout') }}"><i class="fa fa-sign-out fa-fw"></i>{{ "Logout" }}</a>
        </li>
    </ul>
    <!-- /.dropdown-user -->
</li>
<!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->

<div class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ action('UsersController@getDashboard') }}"><i class="fa fa-dashboard fa-fw"></i> {{ "Dashboard" }}</a>
            </li>
        </ul>
        <!-- /#side-menu -->
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            @if(Session::has('message'))
                <p class="alert">{{ Session::get('message') }}</p>
            @endif

            @yield('content')
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

{{ HTML::script('asset/js/jquery.min.js') }}
{{ HTML::script('asset/js/bootstrap.min.js') }}
{{ HTML::script('asset/js/plugins/metisMenu/jquery.metisMenu.js') }}
{{ HTML::script('asset/js/sb-admin.js') }}
@yield('scripts')

</body>

</html>
