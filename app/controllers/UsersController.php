<?php
use Prototype\Forms\User\ChangePassForm;
use Prototype\Forms\User\DetailForm;
use Prototype\Forms\User\RegistrationForm;
use Prototype\Forms\User\SigninForm;
use Prototype\Interfaces\UserRepository;

class UsersController extends BaseController
{
    
    private $userRepository;
    private $registrationForm;
    private $signinForm;
    private $detailForm;
    private $changePassForm;
    
    /**
     * @param UserRepository $userRepository
     * @param RegistrationForm $registrationForm
     * @param SigninForm $signinForm
     * @param DetailForm $detailForm
     * @param ChangePassForm $changePassForm
     */
    public function __construct(
        UserRepository $userRepository,
        RegistrationForm $registrationForm,
        SigninForm $signinForm,
        DetailForm $detailForm,
        ChangePassForm $changePassForm
    ) {
        
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->beforeFilter('auth', array('only' => array('getDashboard')));
        $this->userRepository = $userRepository;
        $this->registrationForm = $registrationForm;
        $this->signinForm = $signinForm;
        $this->detailForm = $detailForm;
        $this->changePassForm = $changePassForm;
    }
    
    public function getIndex()
    {
        
        return Redirect::action("UsersController@getDashboard");
    }
    
    public function getRegister()
    {
        
        return View::make('users.register');
    }
    
    public function postCreate()
    {
        $this->registrationForm->validate(Input::all());
        $this->userRepository->save();
        
        return Redirect::action('UsersController@getLogin')->with('message', 'Thanks for registering!');
    }
    
    public function getLogin()
    {
        
        return View::make('users.login');
    }
    
    public function postSignin()
    {
        $this->signinForm->validate(Input::all());
        if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')))) {
            return Redirect::intended(action('UsersController@getDashboard'))->withMessage('You are now logged in!');
        } else {
            return Redirect::action('UsersController@getLogin')->withMessage('Your username/password combination was incorrect')->withInput();
        }
    }
    
    public function getDashboard()
    {
        
        return View::make('users.dashboard');
    }
    
    public function getLogout()
    {
        Auth::logout();
        
        return Redirect::action('UsersController@getLogin')->withMessage('Your are now logged out!');
    }
    
    public function getDetail()
    {
        
        return View::make("users.detail");
    }
    
    public function postUpdateProfile()
    {
        $this->detailForm->validate(Input::all());
        $this->userRepository->updateProfile();
        
        return Redirect::action('UsersController@getDetail')->withMessage('Profile is successful updated!');
    }
    
    public function getChangePassword()
    {
        
        return View::make("users.change_pass");
    }
    
    public function postChangePassword()
    {
        $this->changePassForm->validate(Input::all());
        $this->userRepository->updatePassword();
        
        return Redirect::action('UsersController@getDetail')->withMessage('Password is successful updated!');
    }
}
